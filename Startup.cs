﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DotNetSMIS.Startup))]
namespace DotNetSMIS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
