﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DotNetSMIS.Models
{
    public class Department
    {
        [Key]
        public int DepartmentId { get; set; }
        [Display(Name = "Department Name")]
        [Required]
        public String DepartmentName { get; set; }
        public String Description { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}